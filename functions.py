import numpy as np 

def f(x):
	return np.sin(x)


def g(x):
	return (x ** 3.) - x - 2.


def y(x):
	return -6 + x + (x ** 2.)


#find the root of a function using the bisection method
def findRoot(function, b1, b2, **keyword_parameters):

	#set tolerance
	if ('tol' in keyword_parameters):
		t = keyword_parameters['tol']
	else:
		t = 1.e-4

	#set maximum number of iterations
	n_max = 1000.

	#check if we are bracketing the root
	n = 0
	while function(b1) * function(b2) >= 0. and n <= 1000.:
		b1 = b1 * 0.50
		b2 = b2 * 1.5
		n = n + 1
	while function(b1) * function(b2) >= 0.:
		print 'The root was not found. Try initial brackets that bracket the root.'

	#start bracketing routine
	n = 0
	while abs(function((b1 + b2) / 2.)) > t and n <= n_max:
		b = (b1 + b2) / 2.
		if function(b) == 0.:
			b1 = b
			b2 = b
		elif function(b1) * function(b) <= 0.:
			b2 = b
		else:
			b1 = b
		n = n + 1

	#print results
	if n == n_max + 1.:
		print 'The root was not found. Try a larger max number of iterations or a smaller tolerance.'
	else:
		root = (b1 + b2) / 2.
		return root
		#print 'root = ' + str(root)
		#print 'function(root) = ' + str(function(root))


#find root of a function using the Newton-Rapshon method
def NR(function, x, **keyword_parameters):
	
	#set tolerance
	if ('tol' in keyword_parameters):
		t = keyword_parameters['tol']
	else:
		t = 1.e-4

	#set maximum number of iterations
	n_max = 1000

	#begin NR routine
	n = 0
	while abs(function(x)) >= t and n <= n_max:
		d = (function(x + 0.0000001) - function(x - 0.0000001)) / 0.0000002
		x = x - function(x) / d
		n = n + 1

	#print results
	if n == n_max + 1.:
		print 'The root was not found. Try a larger max number of iterations or a smaller tolerance.'
	else:
		return x
		#print 'root = ' + str(x)
		#print 'function(root) = ' + str(function(x))


