import random
import math
import numpy as np
import matplotlib.pyplot as plt

#dimension of square room
N = 1

#number of people in room
M = 100

#velocity
v = 0.01 * N

#personal space around a person
p = 0.00

#maximum distance at which someone could get infected
d = 0.05

#probably someone will get infected by sick person
q = 0.25

#number of time steps
t = 1300

#create lists for positions over time
X = [[0 for x in range(M)] for x in range(t)]
Y = [[0 for x in range(M)] for x in range(t)]

#X and Y starting positions of each person
for i in range(M):
	X[0][i] = random.uniform(0, N)
	Y[0][i] = random.uniform(0, N)

#set all people to healty (infected = 0)
infected = [[0 for x in range(M)] for x in range(t)]
for j in range(t):
	for i in range(M):
		infected[j][i] = 0

#infect one person (infected = 1)
#start them in the center
infected[0][0] = 1
X[0][0] = 0.5
Y[0][0] = 0.5

sim_len = []

#start simulation
for j in range(1, t):
	for i in range(M):

		print ''
		print 'Person #' + str(i)
		print 'time step #' + str(j)
		#randomly choose direction
		theta = random.uniform(0, 2 * math.pi)

		#where they will move if possible
		X_temp = X[j-1][i] + v * math.cos(theta)
		Y_temp = Y[j-1][i] + v * math.sin(theta)

		#choose a new direction if this one takes them outside of the room
		while (X_temp > 1 or X_temp < 0) or (Y_temp > 1 or Y_temp < 0):
			#randomly choose direction
			theta = random.uniform(0, 2 * math.pi)

			#where they will move if possible
			X_temp = X[j-1][i] + v * math.cos(theta)
			Y_temp = Y[j-1][i] + v * math.sin(theta)


		#check if new position is within someone else's personal space
		can_we_move = []
		for k in range(M):
			#calculate difference between person i and person k's positions
			X_diff = X[j-1][k] - X_temp
			Y_diff = Y[j-1][k] - Y_temp
			dist = (X_diff ** 2. + Y_diff ** 2.) ** 0.5

			#check if person i is within distance p of person k
			if dist <= p:
				can_we_move.append(1) #too close
			else:
				can_we_move.append(0) #far enough apart

			#check if person k is within p of anyone (besides themselves)
			#if they are, they can't move during this timestep
			if sum(can_we_move) > 1:
				X_temp = X[j-1][i]
				Y_temp = Y[j-1][i]

		#move person to new spot if possible
		#keep them in the same spot if not
		X[j][i] = X_temp
		Y[j][i] = Y_temp

		#check if they are within distance d of someone who is infected (if they are not already infected themselves)
		if infected[j-1][i] == 0:
			print "I'm healthy"
			check = []
			for k in range(M):
				#calculate difference between person i and person k's positions
				X_diff = X[j-1][k] - X[j][i]
				Y_diff = Y[j-1][k] - Y[j][i]
				dist = (X_diff ** 2. + Y_diff ** 2.) ** 0.5

				#check if they are within distance d of sick person
				if dist <= d and infected[j-1][k] == 1:
					check.append(1) #within d of sick person
					print 'I might get infected'
				else:
					check.append(0) #not within d of sick person

			#how many sick people are they within distance d of
			n = sum(check)
			print 'There are this many sick people around me: ' + str(n)

			#if they are within distance d of any sick people, see if they will get infected
			am_i_infected = 0
			for l in range(n):
				#will they get infected by this sick person
				yes_or_no = random.uniform(0, 1)
				if yes_or_no <= q:
					yes_or_no = 1
					print "I will get infected"
				else:
					yes_or_no = 0
				am_i_infected = am_i_infected + yes_or_no

			#set them as infected if they have been infected
			if am_i_infected > 0:
				infected[j][i] = 1
				print 'I got infected'
			else:
				infected[j][i] = 0 #stays healthy
				print "I'm still healthy"

		#if person was alreayd infected, keep them infected
		else:
			infected[j][i] = 1
			print "I'm infected"

	#check to see if everyone is infected
	if sum(infected[j]) == M:
		sim_len.append(j+1)

print sim_len





#plot location of one person to check that they're moving correctly
X_person = []
Y_person = []
for j in range(t):
	X_person.append(X[j][0])
	Y_person.append(Y[j][0])
plt.plot(X_person, Y_person)
plt.xlabel('X')
plt.ylabel('Y')
plt.axis([0., 1., 0., 1.])
plt.savefig('project1_a')
plt.show()

#plot people at start
plt.plot(X[0], Y[0], 'go')
X_infected = []
Y_infected = []
for i in range(M):
	if infected[0][i] == 1:
		X_infected.append(X[0][i])
		Y_infected.append(Y[0][i])
plt.plot(X_infected, Y_infected, 'ro')
plt.xlabel('X')
plt.ylabel('Y')
plt.savefig('project1_b')
plt.show()

#plot people 1/4 through
plt.plot(X[t/4], Y[t/4], 'go')
X_infected = []
Y_infected = []
for i in range(M):
	if infected[t/4][i] == 1:
		X_infected.append(X[t/4][i])
		Y_infected.append(Y[t/4][i])
plt.plot(X_infected, Y_infected, 'ro')
plt.xlabel('X')
plt.ylabel('Y')
plt.savefig('project1_c')
plt.show()

#plot people halfway through
plt.plot(X[t/2], Y[t/2], 'go')
X_infected = []
Y_infected = []
for i in range(M):
	if infected[t/2][i] == 1:
		X_infected.append(X[t/2][i])
		Y_infected.append(Y[t/2][i])
plt.plot(X_infected, Y_infected, 'ro')
plt.xlabel('X')
plt.ylabel('Y')
plt.savefig('project1_d')
plt.show()

#plot people 3/4 through
plt.plot(X[(t*3)/4], Y[(t*3)/4], 'go')
X_infected = []
Y_infected = []
for i in range(M):
	if infected[(t*3)/4][i] == 1:
		X_infected.append(X[(t*3)/4][i])
		Y_infected.append(Y[(t*3)/4][i])
plt.plot(X_infected, Y_infected, 'ro')
plt.xlabel('X')
plt.ylabel('Y')
plt.savefig('project1_e')
plt.show()

#plot people at end
plt.plot(X[t-1], Y[t-1], 'go')
X_infected = []
Y_infected = []
for i in range(M):
	if infected[t-1][i] == 1:
		X_infected.append(X[t-1][i])
		Y_infected.append(Y[t-1][i])
plt.plot(X_infected, Y_infected, 'ro')
plt.xlabel('X')
plt.ylabel('Y')
plt.savefig('project1_f')
plt.show()


#plot number of infected people over time
n_infected = []
time = []
for j in range(t):
	time.append(j)
	total = 0
	for i in range(M):
		total = total + infected[j][i]
	n_infected.append(total)

plt.plot(time, n_infected)
plt.xlabel('Number of time steps')
plt.ylabel('# of people infected')
plt.savefig('project1_g')
plt.show()


