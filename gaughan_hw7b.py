import random
import numpy as np

distribution = []

x = 1000

for i in range(x):

	birthdays = []
	switch = 1
	while switch == 1:
		birthdays.append(random.randrange(1,365))
		for j in range(len(birthdays)-1):
			if birthdays[j] == birthdays[len(birthdays)-1]:
				switch = 0

	distribution.append(len(birthdays))

print np.median(distribution)