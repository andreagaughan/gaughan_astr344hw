
from datetime import datetime 
import math
import numpy as np

from functions import f
from functions import g
from functions import y
from functions import findRoot
from functions import NR

#Part 1
print '************** PART 1 **************'
print ' '

print '** f(x) = sin(x) **'
print ' '
print 'Bracketing Method'
t1 = datetime.now()
print 'root = ' + str(findRoot(f, 3., 4.))
t2 = datetime.now()
print 'calculation time: ' + str(t2 - t1)
print ' '
print 'Newton-Rapshon Method'
t1 = datetime.now()
print 'root = ' + str(NR(f, 3.))
t2 = datetime.now()
print 'calculation time: ' + str(t2 - t1)
print ' '

print '** g(x) = (x^3) - x - 2 **'
print ' '
print 'Bracketing Method'
t1 = datetime.now()
print 'root = ' + str(findRoot(g, 1., 2.))
t2 = datetime.now()
print 'calculation time: ' + str(t2 - t1)
print ' '
print 'Newton-Rapshon Method'
t1 = datetime.now()
print 'root = ' + str(NR(g, 1.))
t2 = datetime.now()
print 'calculation time: ' + str(t2 - t1)
print ' '

print '** y(x) = -6 + x + x^2 **'
print ' '
print 'Bracketing Method'
t1 = datetime.now()
print 'root = ' + str(findRoot(y, 0., 5.))
t2 = datetime.now()
print 'calculation time: ' + str(t2 - t1)
print ' '
print 'Newton-Rapshon Method'
t1 = datetime.now()
print 'root = ' + str(NR(y, 0.))
t2 = datetime.now()
print 'calculation time: ' + str(t2 - t1)
print ' '


#Part 2
print '************** PART 2 **************'
def BB(T):
	h = 6.6260755e-27
	c = 2.99792458e+10
	k = 1.380658e-16
	lamb = 870.e-4
	nu = c / lamb
	B = 1.25e-12
	return ((2. * h * (nu ** 3.)) / (c ** 2.)) / (math.exp((h * nu) / (k * T)) - 1.) - B

print ' '
print 'Bracketing Method'
print 'T = ' + str(findRoot(BB, 5., 400., tol = 10e-22)) + 'K'
print ' '
print 'Newton-Rapshon Method'
print 'T = ' + str(NR(BB, 40., tol = 10e-20)) + 'K'
print ' '
