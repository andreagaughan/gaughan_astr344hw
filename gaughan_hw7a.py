import random
import numpy as np

inside = 0

n = 10000000

for i in range(n):
	x = random.uniform(0,1)
	y = random.uniform(0,1)

	if x ** 2 + y ** 2 < 1.:
		inside = inside + 1

print 'pi  = ' + str(4. * inside / n)
