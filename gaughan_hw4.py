import numpy as np 
import matplotlib.pyplot as plt

# calculate angular diameter (D_A) distance for redshift (z) from 0 to 5

c = 3. * 10. ** 3.
omega_m = 0.3
omega_l = 0.7
omega_R = 0.

z = [x * 0.01 for x in range(0, 502)]
z_np = z[0:-1]

def r(z):
	return c / (omega_m * (1. + z) ** 3. + omega_R * (1. + z) ** 2. + omega_l) ** 0.5

f = []
D_A = []
D_A_np = []
r_arr = []
for i in range(0, len(z) - 1):

	#calculate D_A manually using trapizodal method
	if i == 0:
		f.append((r(z[i]) + r(z[i+1]) / 2.) * 0.01)
	else:
		f.append(f[i-1] + ((r(z[i]) + r(z[i+1])) / 2.) * 0.01)
	z[i] = z[i] + 0.005
	D_A.append(f[i] / (1. + z[i]))

	#calculate D_Z using np.trapz
	r_arr.append(r(z[i]))
	z_arr = z[0:i+1]
	D_A_np.append(np.trapz(r_arr, x=z_arr) / (1. + z_np[i]))
	
z = [0.] + z[0:-1]
D_A = [0.] + D_A


plt.plot(z, D_A, 'b-', z_np, D_A_np, 'r-')
plt.xlabel ('$z$')
plt.ylabel('$D_A$')
plt.xlim(0., 5.)
plt.title('Manual Calculation (blue) & Numpy Function (red)')
plt.show()
