import random
import math
import numpy as np
import matplotlib.pyplot as plt

#dimension of square room
N = 1

#number of people in room
M = 100

#velocity
v = 0.01 * N

#personal space around a person
p = 0.000

#maximum distance at which someone could get infected
d = 0.05

#probably someone will get infected by sick person
q = 0.25

#number of time steps
t = 3000

#run this simulation 100 times
#record how many steps it took to infect everyone
sim = 100
sim_len = []
sim_n = 0
for a in range(sim):
	sim_n = sim_n + 1
	print ' '
	#create lists for positions over time
	X = [[0 for x in range(M)] for x in range(t)]
	Y = [[0 for x in range(M)] for x in range(t)]

	#X and Y starting positions of each person
	for i in range(M):
		X[0][i] = random.uniform(0, N)
		Y[0][i] = random.uniform(0, N)

	#set all people to healty (infected = 0)
	infected = [[0 for x in range(M)] for x in range(t)]
	for j in range(t):
		for i in range(M):
			infected[j][i] = 0

	#infect one person (infected = 1)
	infected[0][0] = 1

	#start simulation
	j = 0
	switch = 1
	while switch == 1:
		j = j + 1
		for i in range(M):

			#randomly choose direction
			theta = random.uniform(0, 2 * math.pi)

			#where they will move if possible
			X_temp = X[j-1][i] + v * math.cos(theta)
			Y_temp = Y[j-1][i] + v * math.sin(theta)

			#choose a new direction if this one takes them outside of the room
			while (X_temp > 1 or X_temp < 0) or (Y_temp > 1 or Y_temp < 0):
				#randomly choose direction
				theta = random.uniform(0, 2 * math.pi)

				#where they will move if possible
				X_temp = X[j-1][i] + v * math.cos(theta)
				Y_temp = Y[j-1][i] + v * math.sin(theta)


			#check if new position is within someone else's personal space
			can_we_move = []
			for k in range(M):
				#calculate difference between person i and person k's positions
				X_diff = X[j-1][k] - X_temp
				Y_diff = Y[j-1][k] - Y_temp
				dist = (X_diff ** 2. + Y_diff ** 2.) ** 0.5

				#check if person i is within distance p of person k
				if dist <= p:
					can_we_move.append(1) #too close
				else:
					can_we_move.append(0) #far enough apart

				#check if person k is within p of anyone (besides themselves)
				#if they are, they can't move during this timestep
				if sum(can_we_move) > 1:
					X_temp = X[j-1][i]
					Y_temp = Y[j-1][i]

			#move person to new spot if possible
			#keep them in the same spot if not
			X[j][i] = X_temp
			Y[j][i] = Y_temp

			#check if they are within distance d of someone who is infected (if they are not already infected themselves)
			if infected[j-1][i] == 0:
				check = []
				for k in range(M):
					#calculate difference between person i and person k's positions
					X_diff = X[j-1][k] - X[j][i]
					Y_diff = Y[j-1][k] - Y[j][i]
					dist = (X_diff ** 2. + Y_diff ** 2.) ** 0.5

					#check if they are within distance d of sick person
					if dist <= d and infected[j-1][k] == 1:
						check.append(1) #within d of sick person
					else:
						check.append(0) #not within d of sick person

				#how many sick people are they within distance d of
				n = sum(check)

				#if they are within distance d of any sick people, see if they will get infected
				am_i_infected = 0
				for l in range(n):
					#will they get infected by this sick person
					yes_or_no = random.uniform(0, 1)
					if yes_or_no <= q:
						yes_or_no = 1
					else:
						yes_or_no = 0
					am_i_infected = am_i_infected + yes_or_no

				#set them as infected if they have been infected
				if am_i_infected > 0:
					infected[j][i] = 1
				else:
					infected[j][i] = 0 #stays healthy

			#if person was alreayd infected, keep them infected
			else:
				infected[j][i] = 1

		print 'Simulation #' + str(sim_n) + '   time step #' + str(j) + '   n infected: ' + str(sum(infected[j]))
		#if all people are infected, end simulation
		if sum(infected[j]) == 100:
			switch = 0
			print 'END'
			sim_len.append(j)

print sim_len

plt.hist(sim_len)
plt.xlabel('# of time steps to infect everyone')
plt.ylabel('Relative freqency')
plt.savefig('project1_g')
plt.show()


