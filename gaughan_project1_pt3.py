import random
import math
import numpy as np
import matplotlib.pyplot as plt

#dimension of square room
N = 1

#number of people in room
M = 100

#velocity
v = 0.01 * N

#personal space around a person
p = 0.000

#maximum distance at which someone could get infected
d = 0.05

#probably someone will get infected by sick person
q = 0.25

#number of time steps
t = 10000

#create lists for positions over time
X = [[0 for x in range(M)] for x in range(t)]
Y = [[0 for x in range(M)] for x in range(t)]

#X and Y starting positions of each person
for i in range(M):
	X[0][i] = random.uniform(0, N)
	Y[0][i] = random.uniform(0, N)

#set all people to healty (infected = 0)
infected = [[0 for x in range(M)] for x in range(t)]
for j in range(t):
	for i in range(M):
		infected[j][i] = 0

#run 100 simulations varying the number of people vaccinated from 0 to 100
sim = 100
sim_len = []
f = []
for a in range(sim):

	#infect one person
	infected[0][0] = 1

	#vaccinate people
	n_vac = a
	for i in range(n_vac):
		infected[0][i+1] = 2

	#start simulation
	j = 0
	switch = 1
	while switch == 1:

		j = j + 1

		if j == t-1:
			switch = 0

		for i in range(M):

			#randomly choose direction
			theta = random.uniform(0, 2 * math.pi)

			#where they will move if possible
			X_temp = X[j-1][i] + v * math.cos(theta)
			Y_temp = Y[j-1][i] + v * math.sin(theta)

			#choose a new direction if this one takes them outside of the room
			while (X_temp > 1 or X_temp < 0) or (Y_temp > 1 or Y_temp < 0):
				#randomly choose direction
				theta = random.uniform(0, 2 * math.pi)

				#where they will move if possible
				X_temp = X[j-1][i] + v * math.cos(theta)
				Y_temp = Y[j-1][i] + v * math.sin(theta)

			#move person to new spot if possible
			#keep them in the same spot if not
			X[j][i] = X_temp
			Y[j][i] = Y_temp

			#check if they are within distance d of someone who is infected (if they are healthy & unvaccinated)
			if infected[j-1][i] == 0:
				check = []
				for k in range(M):
					#calculate difference between person i and person k's positions
					X_diff = X[j-1][k] - X[j][i]
					Y_diff = Y[j-1][k] - Y[j][i]
					dist = (X_diff ** 2. + Y_diff ** 2.) ** 0.5

					#check if they are within distance d of sick person
					if dist <= d and infected[j-1][k] == 1:
						check.append(1) #within d of sick person
					else:
						check.append(0) #not within d of sick person

				#how many sick people are they within distance d of
				n = sum(check)

				#if they are within distance d of any sick people, see if they will get infected
				am_i_infected = 0
				for l in range(n):
					#will they get infected by this sick person
					yes_or_no = random.uniform(0, 1)
					if yes_or_no <= q:
						yes_or_no = 1
					else:
						yes_or_no = 0
					am_i_infected = am_i_infected + yes_or_no

				#set them as infected if they have been infected
				if am_i_infected > 0:
					infected[j][i] = 1
				else:
					infected[j][i] = 0 #stays healthy

			#if person was alreayd infected, keep them infected
			elif infected[j-1][i] == 1:
				infected[j][i] = 1

			#if person was already vaccinated, keep them vaccinated
			else:
				infected[j][i] = 2

		n_infected = 0
		for i in range(M):
			if infected[j][i] == 1:
				n_infected = n_infected +1

		print 'Simulation #' + str(a+1) + '   time step #' + str(j) + '   n infected: ' + str(n_infected)
		#if all people are infected, end simulation
		if n_infected == 100 -a:
			switch = 0
			print 'END'
	sim_len.append(j)

	f.append(a / 100.)

print f
print sim_len

plt.plot(f, sim_len, 'bo')
plt.show()

f = [0.0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.11, 0.12, 0.13, 0.14, 0.15, 0.16, 0.17, 0.18, 0.19, 0.2, 0.21, 0.22, 0.23, 0.24, 0.25, 0.26, 0.27, 0.28, 0.29, 0.3, 0.31, 0.32, 0.33, 0.34, 0.35, 0.36, 0.37, 0.38, 0.39, 0.4, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.47, 0.48, 0.49, 0.5, 0.51, 0.52, 0.53, 0.54, 0.55, 0.56, 0.57, 0.58, 0.59, 0.6, 0.61, 0.62, 0.63, 0.64, 0.65, 0.66, 0.67, 0.68, 0.69, 0.7, 0.71, 0.72, 0.73, 0.74, 0.75, 0.76, 0.77, 0.78, 0.79, 0.8, 0.81, 0.82, 0.83, 0.84, 0.85, 0.86, 0.87, 0.88, 0.89, 0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96, 0.97, 0.98, 0.99]
sim_len1 = [889, 814, 643, 958, 1326, 1344, 1713, 1184, 1521, 1090, 907, 1169, 829, 927, 759, 851, 908, 1051, 2288, 1183, 1145, 1253, 1310, 1099, 1432, 876, 2133, 1256, 630, 1511, 888, 1404, 1497, 1007, 1262, 1604, 1159, 1185, 1643, 1289, 1094, 2170, 1624, 2213, 2221, 1872, 2352, 1878, 1968, 2012, 2900, 1460, 1884, 1071, 2080, 2471, 1574, 1715, 2032, 2474, 3584, 2130, 1795, 2965, 1903, 2900, 3276, 4252, 4974, 2841, 2468, 4585, 2110, 2361, 4431, 2553, 2346, 2928, 2738, 6808, 2221, 3329, 3927, 3568, 2830, 3329, 7256, 2400, 3550, 7270, 9768, 2650, 9999, 4392, 2186, 9994, 9999, 1603, 273, 1]
sim_len2 = [854, 1093, 1367, 1217, 1214, 1227, 1668, 1387, 1609, 1008, 1367, 1160, 1853, 3034, 1448, 1349, 1903, 1433, 1385, 2561, 2307, 2046, 2197, 1170, 1626, 2132, 1550, 1654, 1746, 1137, 1755, 1509, 2052, 1746, 1904, 1051, 1477, 2218, 1658, 2228, 2091, 1801, 2410, 2063, 1271, 1736, 2373, 1720, 2940, 1961, 2215, 3234, 1818, 2316, 1900, 1946, 2754, 2907, 3457, 2495, 2065, 2265, 3151, 2249, 3275, 2764, 1400, 2340, 2713, 3065, 2348, 1692, 4263, 2753, 3428, 2245, 3801, 4140, 3823, 4601, 3864, 2452, 7847, 3364, 8039, 5396, 5141, 9999, 4394, 3273, 4957, 9535, 5715, 3269, 9999, 2350, 5422, 9999, 4497, 1]






