import numpy as np

def stableFunction(x):

	x = np.float64(x)

	if x == 0.:
		return 1.
	elif x == 1.:
		return 1./3.
	else:
		return (13. / 3.) * stableFunction(x - 1.) - (4. / 3.) * stableFunction(x - 2.)


def unstableFunction(x):

	x = np.float32(x)

	if x == 0.:
		return np.float32(1)
	elif x == 1.:
		return np.float32(1./3.)
	else:
		return np.float32(13. / 3.) * unstableFunction(x - 1.) - np.float32(4. / 3.) * unstableFunction(x - 2.)


print 'x(n) = (1/3)^n'
print ' '

n = ['n']
stable = ['Single Precision FP #s']
unstable = ['Floating point #s']
absErr = ['Absolute Error']
relErr = ['Relative Error']

for i in range(0, 21):
	print 'n = ' + str(i)
	print 'Single: ' + str(stableFunction(i))
	print 'FP: ' + str(unstableFunction(i))
	print 'Absolite Error: ' + str(stableFunction(i) - unstableFunction(i))
	print 'Relative Error: ' + str((stableFunction(i) - unstableFunction(i)) / stableFunction(i))
	print ' '





def stableFunction2(x):
	x = np.float64(x)
	return 4. ** x


def unstableFunction2(x):

	x = np.float32(x)

	if x == 0.:
		return np.float32(1)
	elif x == 1.:
		return np.float32(4.)
	else:
		return np.float32(13. / 3.) * unstableFunction2(x - 1.) - np.float32(4. / 3.) * unstableFunction2(x - 2.)


print 'x(n) = 4^n'
print ' '
for i in range(0, 21):
	print "n = " + str(i)
	print '4^n: ' + str(stableFunction2(i))
	print 'recurance relation: ' + str(unstableFunction2(i))
	print "absolute error: " + str(stableFunction2(i) - unstableFunction2(i))
	print "relative error: " + str( (stableFunction2(i) - unstableFunction2(i)) / stableFunction2(i) )
	print " "

print 'This calculation is stable because the result & intermediate steps never go below ~10^-30 or above ~10^30.'
print ' '
print 'Relative error should be used to calculate accuracy and stability because it is not dependednt on the size of the values, only their relative differences.'
print ' '







