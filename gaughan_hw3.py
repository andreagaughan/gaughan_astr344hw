import math
import numpy as np
import matplotlib.pyplot as plt 

# import from model data
# L_model is luminosity in millijanskys
# N_model is the number of galaxies above a luminosity (N>L)
L_model, N_model = np.loadtxt('astr344_homework_materials/model_smg.dat', unpack=True)

# import observed data
# L_obs is the log of luminosty
# dNdL_obs is log(dN/dL) where is N is the number of galaxies above a luminosity
L_obs, dNdL_obs = np.loadtxt('astr344_homework_materials/ncounts_850.dat', unpack=True)

# convert model data N to dN/dL (uneven grid)
dNdL_model = []
for i in range(1, len(L_model) - 1):
	h1 = L_model[i] - L_model[i - 1]
	h2 = L_model[i + 1] - L_model[i]

	dNdL_model.append(abs((h1 / (h2 * (h1 + h2))) * N_model[i + 1] - ((h1 - h2) / (h2 * h1)) * N_model[i] - (h2 / (h1 * (h1 + h2))) * N_model[i - 1]))

# delete first and last value of L_model b/c there is no corresponding dN/dL
L_model = L_model[1:-1]

# take natural log of L_model and dNdL_model to match observed data format
for i in range(0, len(L_model)):
	L_model[i] = math.log10(L_model[i])
	dNdL_model[i] = math.log10(dNdL_model[i])

plt.plot(L_model, dNdL_model, L_obs, dNdL_obs, 'g^', linewidth=3.0)
plt.xlabel('Log(L) (millijanskys)')
plt.ylabel('Log(dN / dL)')
plt.title('Model Data (blue) and Observed Data (green) for Submillimeter-selected Galaxies')
plt.show()