import numpy as np 
import matplotlib.pyplot as plt

true = -20. / 3.

def f(x):
	return x ** 3. + 2. * x ** 2. - 4.

x = [(i * 0.01) - 1. for i in range(0, 201)]

#integrate using piecewise linear method
sum = 0.
for i in range(0, len(x)):
	sum = sum + f(x[i]) * 0.01

print ' '
print 'Piecewise linear method:'
print sum
print 'rel. error: ' + str((true - sum) / true)
print ' '

#integrate using the trapizoidal integrator
f_arr = []
for i in range(0, len(x)):
	f_arr.append(f(x[i]))

sum = np.trapz(f_arr, x=x)
print 'Trapizoidal integrator'
print sum
print 'rel. error: ' + str((true - sum) / true)
print ' '

#integrate using Simpson's Rule
sum = 0.
for i in range(0, len(x)-1):
	a = x[i]
	b = x[i + 1]
	sum = sum + (b - a) / 6. * (f(a) + 4. * f((a + b) / 2.) + f(b))

print "Simpson's rule"
print sum 
print 'rel. error: ' + str((true - sum) / true)
print ' '
