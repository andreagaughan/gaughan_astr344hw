import numpy as np 
import matplotlib.pyplot as plt


#define functions and brackets
def f(x):
	return np.sin(x)
b1_f = 2.
b2_f = 4.

def g(x):
	return (x ** 3.) - x - 2.
b1_g = 1.
b2_g = 2.

def y(x):
	return -6 + x + (x ** 2.)
b1_y = 0.
b2_y = 5.


#create function to find the root of input equation

def findRoot(function, b1, b2):

	#set tolerance
	print ' '
	reply = raw_input('Would you like to use default tolerance of 10.**-4.? (y/n): ')
	if reply == 'y':
		t = 10. ** -4.
	elif reply == 'n':
		t = input('tolerance (in 10.**-4. format) = ')

	#set maximum number of iterations
	reply = raw_input('Would you like to use the default max number of iterations of 100? (y/n): ')
	if reply == 'y':
		n_max = 100.
	elif reply == 'n':
		n_max = input('max number of iterations = ')

	#check if we are bracketing the root
	n = 0
	while function(b1) * function(b2) >= 0. and n <= 1000.:
		b1 = b1 * 0.50
		b2 = b2 * 1.5
		n = n + 1
	while function(b1) * function(b2) >= 0.:
		print 'The root was not found. Try initial brackets that bracket the root.'

	#start bracketing routine
	n = 0
	while abs(function((b1 + b2) / 2.)) > t and n <= n_max:
		b = (b1 + b2) / 2.
		if function(b) == 0.:
			b1 = b
			b2 = b
		elif function(b1) * function(b) <= 0.:
			b2 = b
		else:
			b1 = b
		n = n + 1

	#print results
	if n == n_max + 1.:
		print 'The root was not found. Try a larger max number of iterations or a smaller tolerance.'
	else:
		root = (b1 + b2) / 2.
		print 'root = ' + str(root)
		print 'function(root) = ' + str(function(root))

#find the root of our three functions
print ' '
print 'Find the root of sin(x) between ' + str(b1_f) + ' and ' + str(b2_f) + ':'
findRoot(f, b1_f, b2_f)
print ' '
print 'Find the root of (x^3) - x - 2 between ' + str(b1_g) + ' and ' + str(b2_g) + ':'
findRoot(g, b1_g, b2_g)
print ' '
print 'Find the root of -6 + x + x^2 between ' + str(b1_y) + ' and ' + str(b2_y) + ':'
findRoot(y, b1_y, b2_y)
print ' '

